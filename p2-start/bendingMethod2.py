#!/usr/bin/env python 

# Import necessary modules that will be used 

import os
import sys
import getopt
import matplotlib.pyplot as plotter
import pandas as pd
import numpy as np
import decimal
from openpyxl import Workbook
from datetime import date

def main(argv):
    # Defining the options that users can input into our script...
    try:
        opts, args = getopt.getopt(argv, 'ht:s', ['help', 'tabular-data=', 'save-directory='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="/full/path/to/file" --save-directory="/full/path/to/xls/save/directory"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checking to make sure that the user's option is in our opts list...
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="/full/path/to/file" --save-directory="/full/path/to/xls/save/directory"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # We need to convert our file's (user input) content into a list,
            # because it gets eaten in as a string.
            if (os.path.exists(arg)):
                data_file = arg
            else:
                print('That file does not exist. Please check the file and try again.')
                sys.exit(2)
        elif (opt in ('-s', '--save-directory')):
            if (os.path.exists(arg)):
                save_directory = arg
            else:
                print('The directory you want to save to does not exist. Please create it and then continue.')
                sys.exit(2)
    # If we're running from a GUI, we want to ask for input.
    try:
        data_file
        save_directory
    except:
        data_file = input('Full path to the data you want to plot: ')
        save_directory = input('Full path to the directory you want to save the Excel sheet to: ')
  
  # Read the CSV file, and create a DataFrame (we don't need another DataFrame
    # call to pandas because read_csv does create the DataFrame)
    data = pd.read_csv(data_file, skiprows = 31)
    plot_data(data)
    convert_data_to_xls(data, save_directory, data_file)

def plot_data(data):
    # Change the data type to decimal (just incase it became an object)
    data.astype(decimal.Decimal)
    data.plot('Stress MPa', 'Strain [Exten.] %')
    plotter.title('Stress vs Strain Curve')
    plotter.xlabel('Strain')
    plotter.ylabel('Stress')

    plotter.show()

def convert_data_to_xls(df, save_directory, data_file):
    # Now we need a tuple for our dataframe, so we take df (from the function)
    # and run the columns we need ('Stress MPa' and 'Strain [Exten.] %') through
    # np.column_stack to merge those two columns, and then tuple map it to get
    # a tuple instead of an np array.
    rows = np.column_stack((df['Stress MPa'], df['Strain [Exten.] %']))
    rows = tuple(map(tuple, rows))
    wb = Workbook()
    wb.template = False
    ws = wb.active
    ws['A1'] = 'Stress MPa (x)'
    ws['B1'] = 'Strain [Exten.] % (y)'
    for row in rows:
        ws.append(row)
    today = date.today()
    # Save to our workbook using a unique name (with the basename of the data_file)
    wb.save(save_directory + '/' + today.isoformat() + 'export-' + os.path.basename(os.path.normpath(data_file)) + '.xls')



# Check to make sure that this is not being run by a module before we
# run the main() function.
if (__name__ == "__main__"):
    main(sys.argv[1:])

