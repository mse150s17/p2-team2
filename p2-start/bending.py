#Refer to README file for instructions before beginging Python Programs
#Bending_Plot 

#Import Necessary Modules that will be used 

#Module for Visualization Features      
import matplotlib.pyplot as plt
#Module for Scientific Computing/Graphing 
import numpy as np
#Module for read in data from CSV file  
import csv as csv
#Module which contains command-line arguments 
import sys


readdata = open(sys.argv[1], 'r')
data = []
for row in readdata:
        split = row.split(',')
        data.append([split[3],split[7]])
readdata.close()
writedata = open(sys.argv[2] + '.csv', 'w')
for d in data:
        writedata.write("{},{}".format(d[0],d[1]))
writedata.close()


#reading in the data
#sys.argv[1] reads in argument 1, which in "python plot.py myfilename" is myfilename
#skiprows is skipping past all of the headers to get to the numbers
data = np.loadtxt(sys.argv[1], comments = '"', skiprows = 32, delimiter = ',')

#abs is taking the absolute value of the # column in the data that was read in
stress = abs(data[:,3])
strain = abs(data[:,7])

filename = sys.argv[1]

#def extract_sample(filename):
        #chop off the last four characters of s
        #find the first dash starting from the end and going backwards
#       #print what's left
#       chopped = [0:-4]
#       index = [::-1].find('-')
        #return what's left

#plotting
plt.plot(strain,stress)
plt.xlabel('% Strain')
plt.ylabel('Stress (MPa)')
plt.title(filename)
plt.show()







































































