#Refer to README file for instructions before beginging Python Programs
#Spectrum_Plot

#Import necessary packages that will be used for this program

#Package for visualization features      
import matplotlib.pyplot as plt
#Package for scientific computing/graphing 
import numpy as np
#Package for read in data from CSV file  
import csv as csv
#Package which contains command-line arguments 
import sys


#CONVERTING TO CSV
readdata = open(sys.argv[1], 'r')
data = []
for row in readdata:
        split = row.split('\t')
        data.append([split[0],split[1]])
readdata.close()
writedata = open(sys.argv[2] + '.csv', 'w')
for d in data:
        writedata.write("{},{}".format(d[0],d[1]))
writedata.close()



#PLOTTING
#Reading in the data from the file
#sys.argv[1] reads in argument 1, which in "python plot.py myfilename" is myfilename
data = np.loadtxt(sys.argv[1], delimiter = '\t')

#Assigns the first column of data to wavelength and second to intensity
wavelength = (data[:,0])
intensity = (data[:,1])

filename = sys.argv[1]

#def extract_sample(filename):
	#chop off the last four characters of s
	#find the first dash starting from the end and going backwards
#	#print what's left
#	chopped = [0:-4]
#	index = [::-1].find('-')
	#return what's left
#	return chopped[-index::]
	 

#plots the data
plt.plot(wavelength, intensity)
plt.xlabel('Wavelength (nm)')
plt.ylabel('Intensity')
plt.title(filename)
plt.show()

