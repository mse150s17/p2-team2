# Project 2

This repository has template data files and starter code for the MSE 150 Project #2.

Summary:
	This repository contains sample data that can be analyzed using the Python Software Program. The goal of this project is to design a program that can read tabulated data from a text file.  The program must generate a plot of the data, as well as convert it to a csv file that can be opened in Excel. The sample data consists of quantifiable data that requires two different techniques for analysis.  The python program includes code for both seperated data (bending.py) and tabulated data (spectra.py).   


Analyzing Data Files 


Note: For running the bending tests described below, please use the bendingbackup.py until further notice.

 
Bending Data:
1. Open the file in either vim or a text editor(command line).

Delete all headings as well as any additional specimen data that may be towards the end of the file. Save the new file in the directory with a new filename.  

Once all modifications are completed, type "pwd" to obtain the directory path.   (Never permanently delete raw data!)

2. Type in the command line

 "python bending.py bending/oldfilename bending/newfilename"

 where oldfilename is the name of the file you created in Step 1 and newfilename is the name you would like to name the csv file (.csv is automatically added to the end of newfilename, so no need to include it in your newfilename). 

3. The program will plot a stress vs strain curve based on the measurements from the obtained data files. A csv (excel) file will automatically be generated and saved in the "bending directory" under the new file name.   

 

Spectra data:
1. Enter the command $ "python spectra.py spectra/oldfilename newfilename" where oldfilename is the original filename and newfilename is the name you would like to give the csv file (.csv is automatically added to the end of newfilename, so no need to include it in your newfilename). 
2. A plot of the measured intensity for each wavelength will appear and your csv file will be located in the spectra folder under newfilename. 

The goal of this project is to write a python program to read the data given from text files, generate plots for this data, and create a file that can be opened in Excel.

So the things we need to do are: 
1. Import data sets to plot.py program
2. Turn this data into plots
3. Export this data to a file that can be opened in excel.
=======
>>>>>>> 72285c5d7f7bba0f92418c09cbf6a428516de532
