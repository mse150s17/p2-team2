## Several Repositories for bendingMethod2.py that did not get added to bitbucket are below.  This program is an extra one that actually functions well with the bending data.  The Goal of this file was to integrate the bending.py and spectra.py coding into one. The user would be given the choice of what analysis is done before moving forward with testing.  

#Import necessary packages
import os
import sys
import getopt

def main(argv):
    # Defines the options that users can input into our script
    try:
        opts, args = getopt.getopt(argv, 'ht:', ['help', 'tabular-data='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="/full/path/to/file"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list...
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="/full/path/to/file"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Checks if the "arg" (user input) is a list.
            if (all(isinstance(single_list, list) for single_list in arg)):
                print('success')
            else:
                print('Your tabular data is malformed. Please check the data and try again.')


2

!/usr/bin/python

# Import necessary packages
import os
import sys
import getopt

def main(argv):
    # Defines the options that users can input into our script
    try:
        opts, args = getopt.getopt(argv, 'ht:', ['help', 'tabular-data='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="[1,2,3,4]"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="[1,2,3,4]"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Converts "arg" (user input) into a list,
            # because it gets eaten in as a string.
            if (list(arg)):
                print 'success'

3

#!/usr/bin/python

# Import necessary packages
import os
import sys
import getopt

def main(argv):
    # Defines the options that users can input into our script
    try:
        opts, args = getopt.getopt(argv, 'ht:', ['help', 'tabular-data='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="[1,2,3,4]"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="[1,2,3,4]"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Converts "arg" (user input) into a list,
            # because it gets eaten in as a string.
            if (list(arg)):
                print 'success'

# Checks to make sure that this is the main file before running main()
if (__name__ == "__main__"):
    main(sys.argv[1:])

4 
#!/usr/bin/python

# Import necessary packages
import os
import sys
import getopt

def main(argv):
    # Defines the options that users can input into our script
    try:
        opts, args = getopt.getopt(argv, 'ht:', ['help', 'tabular-data='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="/full/path/to/file"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list...
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="/full/path/to/file"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Converts file's (user input) content into a list,
            # because it gets eaten in as a string.
            f = open(arg)
            if (list(f.read())):
                print 'success'

# Checks to make sure that this is the main file before running main()
if (__name__ == "__main__"):
    main(sys.argv[1:])
5
#!/usr/bin/python

# Import necessary packages
import os
import sys
import getopt

def main(argv):
    # Defines the options that users can input into our script
    try:
        opts, args = getopt.getopt(argv, 'ht:', ['help', 'tabular-data='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="/full/path/to/file"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="/full/path/to/file"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Converts file's (user input) content into a list,
            # because it gets eaten in as a string.
            if (os.path.exists(arg)):
                f = open(arg)
                if (list(f.read())):
                    print('success')
            else:
                print('That file does not exist. Please check the file and try again.')

# Checks to make sure that this is the main file before running main()
if (__name__ == "__main__"):
    main(sys.argv[1:])

nick 

#!/usr/bin/python

# Import necessary packages
import os
import sys
import getopt

def main(argv):
    # Defines the options that users can input into our script
    try:
        opts, args = getopt.getopt(argv, 'ht:', ['help', 'tabular-data='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="/full/path/to/file"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="/full/path/to/file"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Converst file's (user input) content into a list,
            # because it gets eaten in as a string.
            if (os.path.exists(arg)):
                f = open(arg)
                if (list(f.read())):
                    print('success')
            else:
                print('That file does not exist. Please check the file and try again.')

# Checks to make sure that this is the main file before running main()
if (__name__ == "__main__"):
    main(sys.argv[1:])

6) Create new functions for converting to xls and plotting. These functions don't currently
work because of the way pandas is organising the DataFrames. I need to be able to access
the DataFrame properly to get the numerical values to plot on (x,y).

#!/usr/bin/env python

# Import necessary packages
import os
import sys
import getopt
import matplotlib.pyplot as plotter
import pandas
from openpyxl import Workbook
from datetime import date

def main(argv):
    # Defines the options that users can input into our script
    try:
        opts, args = getopt.getopt(argv, 'ht:s', ['help', 'tabular-data=', 'save-directory='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="/full/path/to/file" --save-directory="/full/path/to/xls/save/directory"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="/full/path/to/file" --save-directory="/full/path/to/xls/save/directory"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Converts file's (user input) content into a list,
            # because it gets eaten in as a string.
            if (os.path.exists(arg)):
                data_file = arg
            else:
                print('That file does not exist. Please check the file and try again.')
                sys.exit(2)
        elif (opt in ('-s', '--save-directory')):
            if (os.path.exists(arg)):
                save_directory = arg
            else:
                print('The directory you want to save to does not exist. Please create it and then continue.')
                sys.exit(2)
        data = pandas.read_csv(data_file, skiprows = 31, index_col = ['Strain [Exten.] %', 'Stress MPa'], usecols = ['Strain [Exten.] %', 'Stress MPa'])
        plot_data(pandas.DataFrame(data))
        convert_data_to_xls(data, save_directory, data_file)
        
#Plots stress and strain data
def plot_data(data):
    print(data)
    data.plot()
    plotter.title('Stress vs Strain Curve')
    plotter.xlabel('Strain')
    plotter.ylabel('Stress')

    plotter.show()

#Converts data to Excel file
def convert_data_to_xls(data, save_directory, data_file):
    wb = Workbook()
    wb.template = False
    ws = wb.active
    ws['A1'] = 'Strain [Exten.] %'
    ws['A2'] = 'Stress MPa'
    ws.append(data)
    today = date.today()
    wb.save(save_directory + '/' + today.isoformat + 'export-' + data_file + '.xls')



# Checks to make sure that this is not being run by a module before we
# run the main() function.
if (__name__ == "__main__"):
    main(sys.argv[1:])

7) The matplotlib was throwing a few errors with the datasets given in class, but upon testing them with a smaller chunk of the same datasets it appears to be working properly. Just incase, I've added 'data.astype(decimal.Decimal)' to change the datatype to a decimal in the event it gets put in as a string.

8) dataframe I previously took from the main() function was not working because it was not a tuple, so I've modified the data (using np.column_stack and mapping) to make it a tuple so that we can add it to our workbook via openpyxl

!/usr/bin/env python

# Import necessary packages
import os
import sys
import getopt
import matplotlib.pyplot as plotter
import pandas as pd
import numpy as np
import decimal
from openpyxl import Workbook
from datetime import date

 # Defines the options that users can input into our script
def main(argv):
   
    try:
        opts, args = getopt.getopt(argv, 'ht:s', ['help', 'tabular-data=', 'save-directory='])
    except getopt.GetoptError:
        print('usage: [--help] [--tabular-data="/full/path/to/file" --save-directory="/full/path/to/xls/save/directory"]')
        sys.exit(2)

    for opt, arg in opts:
        # Checks to make sure that the user's option is in our opts list...
        if (opt in ('-h', '--help')):
            print('usage: [--help] [--tabular-data="/full/path/to/file" --save-directory="/full/path/to/xls/save/directory"]')
            sys.exit()
        elif (opt in ('-t', '--tabular-data')):
            # Converts file's (user input) content into a list,
            # because it gets eaten in as a string.
            if (os.path.exists(arg)):
                data_file = arg
            else:
                print('That file does not exist. Please check the file and try again.')
                sys.exit(2)
        elif (opt in ('-s', '--save-directory')):
            if (os.path.exists(arg)):
                save_directory = arg
            else:
                print('The directory you want to save to does not exist. Please create it and then continue.')
                sys.exit(2)
    data = pd.read_csv(data_file, skiprows = 31)
    plot_data(data)
    convert_data_to_xls(save_directory, data_file)
    
#Plots stress and strain data
def plot_data(data):
    data.astype(decimal.Decimal)
    data.plot('Stress MPa', 'Strain [Exten.] %')
    plotter.title('Stress vs Strain Curve')
    plotter.xlabel('Strain')
    plotter.ylabel('Stress')

    plotter.show()
    
#Converts data to an Excel file
def convert_data_to_xls(save_directory, data_file):
    df = pd.read_csv(data_file, skiprows = 31, dtype = tuple)
    rows = np.column_stack((df['Stress MPa'], df['Strain [Exten.] %']))
    rows = tuple(map(tuple, rows))
    wb = Workbook()
    wb.template = False
    ws = wb.active
    ws['A1'] = 'Stress MPa (x)'
    ws['B1'] = 'Strain [Exten.] % (y)'
    for row in rows:
        ws.append(row)
    today = date.today()
    wb.save(save_directory + '/' + today.isoformat() + 'export-' + os.path.basename(os.path.normpath(data_file)) + '.xls')



# Checks to make sure that this is not being run by a module before we
# run the main() function.
if (__name__ == "__main__"):
    main(sys.argv[1:])

9) I've also removed the extra call to pd.read_csv, because we can use the dataframe we got in the main() function (basically the same). So instead, we send that dataframe over to our xls function


# Checks to make sure that this is not being run by a module before we
# run the main() function.
if (__name__ == "__main__"):
    main(sys.argv[1:])
