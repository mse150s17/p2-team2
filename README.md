PROJECT 2

This repository has some template data files and starter code for our Spring 2017 project #2.

Summary: 
This repository contains sample data; comma separated data in bending/ and tabulated data in spectra/. This repository also contains python programs to plot the data as well as convert it to a csv format which can be opened in Excel. Because the sample data for spectra is formatted differently than the bending data, each has its own python program, titled after its respectful test (spectra.py and bending.py).

Running Tests:
To analyze bending data: 

Delete all headings as well as any additional specimen data that may be towards the end of the file. Save as a new file. (Never permanently delete raw data!) To save as a new file while in vim, use the command :w newfilename 

2. Type in the command $ python bending.py bending/oldfilename bending/newfilename where oldfilename is the name of the file you created in Step 1 and newfilename is the name you would like to name the csv file (.csv is automatically added to the end of newfilename, so no need to include it in your newfilename). 

3. A plot of the stress and strain measurements from the bending test will appear and your csv file will be located in the bending folder under newfilename.

To analyze spectra data: 

1. Enter the command $ python spectra.py spectra/oldfilename newfilename where oldfilename is the original filename and newfilename is the name you would like to give the csv file (.csv is automatically added to the end of newfilename, so no need to include it in your newfilename). 

2. A plot of the measured intensity for each wavelength will appear and your csv file will be located in the spectra folder under newfilename.